/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_H
#define __BSP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "cx32l003_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "io_mapping.h"
#include "log.h"
#include "util.h"
/* USER CODE END Includes */

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

#define SPI_NSS_PIN			GPIO_PIN_4
#define SPI_NSS_GPIO		GPIOC
#define SPI_MOSI_PIN		GPIO_PIN_6
#define SPI_MOSI_GPIO		GPIOC
#define SPI_MISO_PIN		GPIO_PIN_2
#define SPI_MISO_GPIO		GPIOD
#define SPI_CLK_PIN			GPIO_PIN_5
#define SPI_CLK_GPIO		GPIOC
#define IRQ_PIN			    GPIO_PIN_4
#ifdef MS1658_BOARD
    #define IRQ_GPIO		    GPIOD
#else //default 
    #define IRQ_GPIO		    GPIOB
#endif
#define LED_PIN			    GPIO_PIN_1
#define LED_GPIO		    GPIOA

#define ADCx		ADC
#define ADC_TIMEOUT_MAX		0xFFFFFFFF
#define __HAL_RCC_ADCxAINx_GPIO_CLK_ENABLE()		__HAL_RCC_GPIOC_CLK_ENABLE()
#define	ADCx_AINx_PIN                               GPIO_PIN_3
#define	ADCx_AINx_PORT                              GPIOC


/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

void BSP_Init(void);
void SPIM_Init(void);
void LPTIM_Init(unsigned short period);
extern void LPTIM_TimeOut_Reset(void);
extern void mg_activate(unsigned char value);
extern unsigned char mg_writeBuf(unsigned char reg, unsigned char const *pBuf, unsigned char len);

unsigned int GetSysTickCount(void);
void IrqMcuGotoSleepAndWakeup(void);

void LED_ONOFF(unsigned char OnOff);//for ui use

uint16_t GetADCValue(void);

/* Private defines -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */

