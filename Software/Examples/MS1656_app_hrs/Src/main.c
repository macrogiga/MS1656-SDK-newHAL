#include "BSP.h"
#include "mg_api.h"


unsigned char *ble_mac_addr;


const unsigned char AdvDat_HRS[]=
{
    0x02,0x01,0x06,
    0x03,0x19,0x41,0x03,
    0x05,0x03,0x0D,0x18,0x0A,0x18
};

int main( void )
{
    unsigned char * const ft_val = (unsigned char *)0x18000040;
    unsigned char ft_value[2] = {0xc0, 0x12};
    
    BSP_Init();
    
    SetBleIntRunningMode();
    radio_initBle(TXPWR_0DBM, &ble_mac_addr);
    
    if((*ft_val > 11) && (*ft_val < 25)){
        ft_value[1] = *ft_val;
        mg_activate(0x53);
        mg_writeBuf(0x4, ft_value, 2);
        mg_activate(0x56);
    }
    
    ble_set_adv_data((unsigned char *)AdvDat_HRS, sizeof(AdvDat_HRS));
    
    ble_run_interrupt_start(160*2); //320*0.625=200 ms
    
    while(1)
    {
        IrqMcuGotoSleepAndWakeup();
    }
}

