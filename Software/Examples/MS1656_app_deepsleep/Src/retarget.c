/*
    Copyright (c) 2020 Shanghai Macrogiga Electronics Co., Ltd.

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
*/
#include "BSP.h"
#include "mg_api.h"


#define MAX_SIZE 200
u8 txBuf[MAX_SIZE], rxBuf[MAX_SIZE];
static u16 RxCont = 0;
static u8 PosW = 0, txLen = 0;
unsigned int StandbyTimeout;

extern char GetConnectedStatus(void);

void UART0_IRQHandler(void)
{
    if((UARTx->INTSR&UART_INTSR_TI)==UART_INTSR_TI)
    {
        UARTx->INTCLR |= UART_INTCLR_TICLR_Msk;
        if (PosW < txLen)
        {
            UARTx->SBUF = txBuf[PosW++];
        }
        
        if (PosW >= txLen){
            txLen = 0;
            PosW = 0;
        }
    } 
    else if((UARTx->INTSR&UART_INTSR_RI)==UART_INTSR_RI)
    {
        UARTx->INTCLR |= UART_INTCLR_RICLR_Msk;
        rxBuf[RxCont] = UARTx->SBUF;
        RxCont++;
        if(RxCont >= MAX_SIZE)
        {
            RxCont = 0;
        }
    }
}

void moduleOutData(u8*data, u8 len) //api
{
    unsigned char i;

    if ((txLen+len)<MAX_SIZE)//buff not overflow
    {
        for (i=0;i<len;i++)
        {
            txBuf[txLen+i] = *(data+i);
        }
        txLen += len;
    }
}
#define comrxbuf_wr_pos RxCont
u16 comrxbuf_rd_pos = 0; //init, com rx buffer
void CheckComPortInData(void) //at cmd NOT supported
{
    u16 send;
    
    if(comrxbuf_wr_pos != comrxbuf_rd_pos)//not empty
    {
        if(!GetConnectedStatus())
        {
            comrxbuf_rd_pos = comrxbuf_wr_pos; //empty the buffer if any
        }
        else //connected
        {
            if(comrxbuf_wr_pos > comrxbuf_rd_pos)
            {
                send = sconn_notifydata(rxBuf+comrxbuf_rd_pos,comrxbuf_wr_pos - comrxbuf_rd_pos);
                comrxbuf_rd_pos += send;
            }
            else 
            {
                send = sconn_notifydata(rxBuf+comrxbuf_rd_pos,MAX_SIZE - comrxbuf_rd_pos);
                comrxbuf_rd_pos += send;
                comrxbuf_rd_pos %= MAX_SIZE;
            }
        }
    }
}


void UsrProcCallback(void) //porting api
{
    static uint8_t cnt_flash = 0;
    
    LPTIM_TimeOut_Reset();
    
    if(GetConnectedStatus()){//connected
        StandbyTimeout = 0;
    }else{
        StandbyTimeout ++;
    }
    
    cnt_flash++;
    LED_ONOFF(cnt_flash & 0x1);
    
    CheckComPortInData();
    if ((txLen) && (0 == PosW))
    {
        UARTx->SBUF = txBuf[PosW++];
    }
}
