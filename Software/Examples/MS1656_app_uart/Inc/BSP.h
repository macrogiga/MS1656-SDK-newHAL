/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_H
#define __BSP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "cx32l003_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "io_mapping.h"
#include "log.h"
#include "util.h"
/* USER CODE END Includes */

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

#define SPI_NSS_PIN			GPIO_PIN_4
#define SPI_NSS_GPIO		GPIOC
#define SPI_MOSI_PIN		GPIO_PIN_6
#define SPI_MOSI_GPIO		GPIOC
#define SPI_MISO_PIN		GPIO_PIN_2
#define SPI_MISO_GPIO		GPIOD
#define SPI_CLK_PIN			GPIO_PIN_5
#define SPI_CLK_GPIO		GPIOC
#define IRQ_PIN			    GPIO_PIN_4
#ifdef MS1658_BOARD
    #define IRQ_GPIO		    GPIOD
#else //default 
    #define IRQ_GPIO		    GPIOB
#endif
#define LED_PIN			    GPIO_PIN_1
#define LED_GPIO		    GPIOA

#define UARTx								UART0
#define BAUDRATE							115200
#ifdef MS1658_BOARD
    #define UARTx_TX_PORT						GPIOA
    #define UARTx_TX_PIN						GPIO_PIN_2
#else //default
    #define UARTx_TX_PORT						GPIOD
    #define UARTx_TX_PIN						GPIO_PIN_4
#endif
#define UARTx_RX_PORT						GPIOD
#define UARTx_RX_PIN						GPIO_PIN_3
#define UARTx_TX_ALTERNATE_AFn				GPIO_AF5_UART0_TXD
#define UARTx_RX_ALTERNATE_AFn				GPIO_AF5_UART0_RXD
#define UARTx_IRQn							UART0_IRQn

#define __HAL_RCC_UARTx_CLK_ENABLE()  		__HAL_RCC_UART0_CLK_ENABLE()
#ifdef MS1658_BOARD
    #define __HAL_RCC_UARTx_GPIO_CLK_ENABLE()  	__HAL_RCC_GPIOA_CLK_ENABLE();__HAL_RCC_GPIOD_CLK_ENABLE()
#else //default
    #define __HAL_RCC_UARTx_GPIO_CLK_ENABLE()  	__HAL_RCC_GPIOD_CLK_ENABLE()
#endif

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

void BSP_Init(void);
void SPIM_Init(void);
void LPTIM_Init(unsigned short period);
extern void LPTIM_TimeOut_Reset(void);
extern void mg_activate(unsigned char value);
extern unsigned char mg_writeBuf(unsigned char reg, unsigned char const *pBuf, unsigned char len);

unsigned int GetSysTickCount(void);
void IrqMcuGotoSleepAndWakeup(void);

void LED_ONOFF(unsigned char OnOff);//for ui use


/* Private defines -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */
