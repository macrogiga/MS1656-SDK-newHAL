/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_H
#define __BSP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "cx32l003_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "io_mapping.h"
#include "log.h"
#include "util.h"
/* USER CODE END Includes */

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

#define SPI_NSS_PIN			GPIO_PIN_4
#define SPI_NSS_GPIO		GPIOC
#define SPI_MOSI_PIN		GPIO_PIN_6
#define SPI_MOSI_GPIO		GPIOC
#define SPI_MISO_PIN		GPIO_PIN_2
#define SPI_MISO_GPIO		GPIOD
#define SPI_CLK_PIN			GPIO_PIN_5
#define SPI_CLK_GPIO		GPIOC
#define IRQ_PIN			    GPIO_PIN_4
#define IRQ_GPIO		    GPIOB


//4 input gpios for application
#define CONFIG_PIN			GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5
#define PORT_PIN			GPIO_PIN_3
#define ATMODE_PIN			GPIO_PIN_4
#define BAUD_PIN			GPIO_PIN_5
#define CONFIG_GPIO		    GPIOD

#define WAKEUP_PIN			GPIO_PIN_6
#define WAKEUP_GPIO		    GPIOD

//3 output gpios for application
#define CONN_PIN			GPIO_PIN_3
#define CONN_GPIO		    GPIOA
#define I2CREQ_PIN			GPIO_PIN_5
#define I2CREQ_GPIO		    GPIOB
#define STANDBY_PIN			GPIO_PIN_3
#define STANDBY_GPIO		GPIOC

//uart/i2c use GPIOA1&2
#define UARTx								UART0
#define UARTx_TX_PORT						GPIOA
#define UARTx_TX_PIN						GPIO_PIN_2
#define UARTx_RX_PORT						GPIOA
#define UARTx_RX_PIN						GPIO_PIN_1
#define UARTx_TX_ALTERNATE_AFn				GPIO_AF5_UART0_TXD
#define UARTx_RX_ALTERNATE_AFn				GPIO_AF5_UART0_RXD
#define UARTx_IRQn							UART0_IRQn

#define __HAL_RCC_UARTx_CLK_ENABLE()  		__HAL_RCC_UART0_CLK_ENABLE()
#define __HAL_RCC_UARTx_GPIO_CLK_ENABLE()  	__HAL_RCC_GPIOA_CLK_ENABLE()

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

void BSP_Init(void);
void SPIM_Init(void);
void LPTIM_Init(unsigned short period);
extern void LPTIM_TimeOut_Reset(void);
extern void mg_activate(unsigned char value);
extern unsigned char mg_writeBuf(unsigned char reg, unsigned char const *pBuf, unsigned char len);

unsigned int GetSysTickCount(void);
void IrqMcuGotoSleepAndWakeup(void);

//void LED_ONOFF(unsigned char OnOff);//for ui use


/* Private defines -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* __BSP_H */
