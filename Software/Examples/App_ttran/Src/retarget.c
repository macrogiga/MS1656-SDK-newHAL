/*
    Copyright (c) 2020 Shanghai Macrogiga Electronics Co., Ltd.

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
*/
#include "BSP.h"
#include "mg_api.h"

extern unsigned char AtcmdFlag;
extern unsigned char SleepStop;
extern unsigned char *ble_mac_addr;

#define MS1656_UART_VERSION_STRING  "IND:Ver1.0\n"
#define MAX_SIZE 500
u8 txBuf[MAX_SIZE], rxBuf[MAX_SIZE];
u16 RxCont = 0;
u8 PosW = 0, txLen = 0;

extern char GetConnectedStatus(void);

extern I2C_HandleTypeDef i2c_test;

void atcmd_Help(u8* parameter,u16 len);

/**
  * @brief This function handles I2C Interrupt .
  */
void I2C_IRQHandler(void)
{
    u32 i2c_flag = 0;
    
    HAL_I2C_Wait_Flag(&i2c_test, &i2c_flag);
    if(i2c_flag == I2C_FLAG_SLAVE_RX_SLAW_ACK) 
    {
        HAL_NVIC_DisableIRQ(I2C_IRQn);
        HAL_I2C_Slave_Receive(&i2c_test,rxBuf,&RxCont);
        HAL_NVIC_EnableIRQ(I2C_IRQn);
    }
    else if(i2c_flag == I2C_FLAG_SLAVE_TX_SLAW_ACK) 
    {
        HAL_NVIC_DisableIRQ(I2C_IRQn);
        HAL_I2C_Slave_Transmit(&i2c_test,txBuf,txLen);
        HAL_GPIO_WritePin(I2CREQ_GPIO,I2CREQ_PIN,GPIO_PIN_RESET);
        HAL_NVIC_EnableIRQ(I2C_IRQn);
    }
}

/**
  * @brief This function handles UART0 Interrupt .
  */
void UART0_IRQHandler(void)
{
    if((UARTx->INTSR&UART_INTSR_TI)==UART_INTSR_TI)
    {
        UARTx->INTCLR |= UART_INTCLR_TICLR_Msk;
        if (PosW < txLen)
        {
            UARTx->SBUF = txBuf[PosW++];
        }
        
        if (PosW >= txLen){
            txLen = 0;
            PosW = 0;
        }
    } 
    else if((UARTx->INTSR&UART_INTSR_RI)==UART_INTSR_RI)
    {
        UARTx->INTCLR |= UART_INTCLR_RICLR_Msk;
        rxBuf[RxCont] = UARTx->SBUF;
        RxCont++;
        if(RxCont >= MAX_SIZE)
        {
            RxCont = 0;
        }
    }
}

void moduleOutData(u8*data, u8 len) //api
{
    unsigned char i;

    if ((txLen+len)<MAX_SIZE)//buff not overflow
    {
        for (i=0;i<len;i++)
        {
            txBuf[txLen+i] = *(data+i);
        }
        txLen += len;
    }
}

#define comrxbuf_wr_pos RxCont
u16 comrxbuf_rd_pos = 0; //init, com rx buffer

//500 Byte
#define MAX_SEND_LEN  200
#define MAX_AT_CMD_BUF_SIZE (19+2*MAX_SEND_LEN)
u8 AtCmdBuf[MAX_AT_CMD_BUF_SIZE];
u8 TempBuf[32];
u16 AtCmdBufDataSize=0;

static unsigned char Str2Hex(unsigned char *pt)
{
    unsigned char i=0;
    char result=0;
    
    while(1)
    {
        if(*(pt+i) <= '9')
        {
            result |=  *(pt+i) - '0';
        }
        else if(*(pt+i) <= 'F')
        {
            result |=  *(pt+i) - 'A' + 10;
        }
        else
        {
            result |=  *(pt+i) - 'a' + 10;
        }
        
        if (0 == i)
        {
            result <<= 4;
        }
        else
        {
            break;
        }
        i++;
    }
    return result;
}

static void Hex2Str(unsigned char *s1,unsigned char *s2,unsigned char len)
{ 
    unsigned char i;
    
    for (i=0;i<len;i++)
    {
        s2[i*2] = (*(s1+i) & 0xF0)>>4;
        if(s2[i*2] < 10) 
            s2[i*2]=s2[i*2]+'0'; //��0--9��
        else  
            s2[i*2]=s2[i*2]+'A'-10; //��A--F��
            
            
        s2[i*2+1] = *(s1+i) & 0x0F;
        if(s2[i*2+1] < 10) 
            s2[i*2+1]=s2[i*2+1]+'0'; //��0--9��
        else  
            s2[i*2+1]=s2[i*2+1]+'A'-10; //��A--F��
    }
}
//AT+SETNAME=abcdefg
void atcmd_SetName(u8* parameter,u16 len)
{   
    if((len <= 8)||(len > 28)){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    updateDeviceInfoData(parameter+8,len-8);
    moduleOutData((u8*)"IND:OK\n",7);
}
//AT+SETADV=0xabcdefg
void atcmd_SetAdv(u8* parameter,u16 len)
{
    u8 i = 0;
    if(len <= 7){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    if (len > 9)
    {
        for (i=0;i<len-9;i+=2)
        {
            TempBuf[i>>1] = Str2Hex(parameter+9+i);
        }
    }
    
    ble_set_adv_data(TempBuf,i>>1);
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+SETRSP=0xabcdefg
void atcmd_SetRsp(u8* parameter,u16 len)
{   
    u8 i = 0;
    if(len <= 7){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    if (len > 9)
    {
        for (i=0;i<len-9;i+=2)
        {
            TempBuf[i>>1] = Str2Hex(parameter+9+i);
        }
    }
    
    ble_set_adv_rsp_data(TempBuf,i>>1);
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+SETTYPE=0xXX
void atcmd_SetType(u8* parameter,u16 len)
{   
    if(len < 12){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    TempBuf[0] = Str2Hex(parameter+10);

    ble_set_adv_type(TempBuf[0]);
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+SETMAC=0xXXXXXXXXXXXX
void atcmd_SetMac(u8* parameter,u16 len)
{
    u8 i = 0;
    if(len < 21){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    if (len > 9)
    {
        for (i=0;i<len-9;i+=2)
        {
            TempBuf[i>>1] = Str2Hex(parameter+9+i);
        }
    }
    radio_setBleAddr(TempBuf);
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+SETADVINTERV=160
void atcmd_SetAdvInterv(u8* parameter,u16 len)
{
    u16 adv_interv=0;
    u8 i;
    if(len <= 13){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    for (i=13;i<len;i++)
    {
        adv_interv *= 10;
        adv_interv += (parameter[i] - '0');
    }
    
    if(adv_interv < 16)
        adv_interv = 16;
    if(adv_interv > 3200)
        adv_interv = 3200;
    
    ble_set_interval(adv_interv);
    moduleOutData((u8*)"IND:OK\n",7);
}

void atcmd_GetAdv(u8* parameter,u16 len)
{
    
}
void atcmd_GetRsp(u8* parameter,u16 len)
{
    
}
void atcmd_GetType(u8* parameter,u16 len)
{
    
}

void atcmd_GetAdvInterv(u8* parameter,u16 len)
{
    
}

//AT+GETMAC
void atcmd_GetMac(u8* parameter,u16 len)
{
    u8 tab[12];
    if(len != 6){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    Hex2Str(ble_mac_addr,tab,6);
    moduleOutData((u8*)"IND:MAC=",8);
    moduleOutData(tab,12);
    moduleOutData((u8*)"\n",1);
}

//AT+BLESEND=N,0x123456abef
u8 send_data[MAX_SEND_LEN] = {0};
static u16 sent_num = 0, send_len = 0;
void atcmd_SendData(u8* parameter, u16 len)
{
//    u8* data = send_data;
    u16 data_len = 0,i;
    u16 len_datalen = 0;
    
    //I am here check the data format
    if(len <= 8){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    if(0 == GetConnectedStatus()){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    parameter += 8;//move to N
    while(parameter[0] != ',')
    {
        data_len *= 10;
        data_len += (parameter[0] - '0');
        
        parameter ++;
        len_datalen ++;
    }
    
    if(data_len > MAX_SEND_LEN){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    parameter += 3; //move 0x

    len_datalen = (len - 11 - len_datalen)/2;

    send_len += data_len;
    if (send_len > MAX_SEND_LEN)
        send_len -= MAX_SEND_LEN;
    
    for(i = 0 ; i < len_datalen ; i++) 
    {
        send_data[(sent_num+i)%MAX_SEND_LEN] = Str2Hex(parameter);
        parameter += 2;
    }
    for(i=len_datalen; i<data_len; i++) //data pad with 0
    {
        send_data[(sent_num+i)%MAX_SEND_LEN] = 0x30;
    }
    
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+UPDATE=
void atcmd_Update(u8* parameter,u16 len)
{
    u16 tab[4]={0,0,0,0};
    u8 i,j=0;
    if(len <= 7){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    if(0 == GetConnectedStatus()){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
        
    for (i=7;i<len;i++)
    {
        if (parameter[i] != ',')
        {
            tab[j] *= 10;
            tab[j] += (parameter[i] - '0');
        }
        else
        {
            j++;
        }
    }

    SIG_ConnParaUpdateReq(tab[0], tab[1], tab[2], tab[3]);

    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+GETCONINTERV
void atcmd_GetConInterv(u8* parameter,u16 len)
{
    u16 conn_interv;
    u8 data;
    if(len != 12){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    if(0 == GetConnectedStatus()){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }

    conn_interv = sconn_GetConnInterval();
    if (conn_interv > 1000)
    {
        data = conn_interv/1000 + '0';
        moduleOutData(&data,1);
    }
    if (conn_interv > 100)
    {
        data = (conn_interv%1000)/100 + '0';
        moduleOutData(&data,1);
    }
    if (conn_interv > 10)
    {
        data = (conn_interv%100)/10 + '0';
        moduleOutData(&data,1);
    }
    data = conn_interv%10 + '0';
    
    moduleOutData((u8*)"IND:CON=",8);
    moduleOutData(&data,1);
    moduleOutData((u8*)"\n",1);
}

//AT+DISCON, disconnect the connection is any
void atcmd_DisconnecteBle(u8* parameter,u16 len)
{
    if(len != 6){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    ble_disconnect();
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+SETADVEN=x		x=0/1, 0 disable adv, 1 enable adv
void atcmd_SetAdvEn(u8* parameter,u16 len)
{
    if(len <= 9){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    parameter += 9;//move to x
    
    if('0' == parameter[0]) //diable adv
    {
        ble_set_adv_enableFlag(0);
    }
    else
    {
        ble_set_adv_enableFlag(1);
    }
    
    moduleOutData((u8*)"IND:OK\n",7);
}

//AT+LOWPOWER=x		x=0/1
void atcmd_LowPower(u8* parameter,u16 len)
{
    if(len <= 9){
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
        
    parameter += 9;//move to x
    
    if (0x31 == parameter[0])
    {
        SleepStop = 1;
    }
    else if (0x30 == parameter[0])
    {
        SleepStop = 0;
    }
    else
    {
        moduleOutData((u8*)"IND:ERR\n",8);
        return;
    }
    
    moduleOutData((u8*)"IND:OK\n",7);
}

#define MAX_AT_CMD_NAME_SIZE 16
typedef void (*ATCMDFUNC)(u8* cmd/*NULL ended, leading with the cmd NAME string, checking usage*/,u16 len);    
typedef struct _tagATCMD
{
    ATCMDFUNC func;
    u8 name[MAX_AT_CMD_NAME_SIZE]; //max len is 11 bytes
}AT_CMD_FUNC;

//function list
AT_CMD_FUNC at_func_list[]=
{
    {atcmd_SetName,         "SETNAME="},
    {atcmd_SetAdv,          "SETADV="},
    //{atcmd_GetAdv,          "GETADV"},
    {atcmd_SetRsp,          "SETRSP="},
    //{atcmd_GetRsp,          "GETRSP"},
    {atcmd_SetType,         "SETTYPE="},
    //{atcmd_GetType,         "GETTYPE"},
    {atcmd_SetMac,          "SETMAC="},
    {atcmd_GetMac,          "GETMAC"},
    {atcmd_SetAdvInterv,    "SETADVINTERV="},
    //{atcmd_GetAdvInterv,    "GETADVINTERV"},
    {atcmd_SendData,        "BLESEND="},
    {atcmd_Update,          "UPDATE="},
    {atcmd_GetConInterv,    "GETCONINTERV"},
    {atcmd_DisconnecteBle,  "DISCON"},
    {atcmd_LowPower,        "LOWPOWER="},
    {atcmd_SetAdvEn,        "SETADVEN="},
    {atcmd_Help,            "HELP"},
};

#define at_cmd_num  (sizeof(at_func_list)/sizeof(at_func_list[0]))

static u8 IsExactCmdInclude(u8* data, const u8* cmd)
{
    u8 i,len = strlen((const char*)cmd);
    
    for(i = 0 ; i < len ; i ++)
    {        
        if(cmd[i] != data[i])
        {
            return 0;
        }
    }
    
    return 1;
}

//AT+HELP
void atcmd_Help(u8* parameter,u16 len)
{
    u8 i,templen;
    u8 name[20]="AT+";
    
    moduleOutData((u8*)"IND:OK\n",7);
    
    moduleOutData((u8*)MS1656_UART_VERSION_STRING,sizeof(MS1656_UART_VERSION_STRING)-1);
    
    for(i = 0; i < at_cmd_num ; i ++)
    {
        strcpy((char*)name+3,((char*)at_func_list[i].name));
        templen = strlen((char*)name);
        name[templen] = 0X0A;
        moduleOutData((u8*)name,templen+1); //NULL terminated format
    }
}

static void AtCmdParser(u8* cmd, u16 len)
{
    u8 i;
    
    for(i = 0 ; i < at_cmd_num ; i++)
    {
        if(IsExactCmdInclude(cmd,at_func_list[i].name))
        {
            //found
            at_func_list[i].func(cmd/*including the CMD name*/,len);
            
            return;
        }
    }

    moduleOutData((u8*)"IND:ERR\n",8);
}


static void AtCmdPreParser(u8* cmd, u16 len)
{
    if(!IsExactCmdInclude(cmd, (const u8*)"AT+")) //AT+MINFO
    {
        moduleOutData((u8*)"IND:ERR\n",8);
        return; //cmd error
    }
    
    AtCmdParser(cmd+3,len-3);
}

void CheckAtCmdInfo(void) //main entrance
{
    while(comrxbuf_wr_pos != comrxbuf_rd_pos) //not empty
    {
        //has data
        AtCmdBuf[AtCmdBufDataSize++] = rxBuf[comrxbuf_rd_pos];
        
        if(AtCmdBufDataSize >= MAX_AT_CMD_BUF_SIZE)//error found
        {
            AtCmdBufDataSize = 0;//I just reset the position, drop the cmd
        }
        
        if((rxBuf[comrxbuf_rd_pos] == 0x0a) || (rxBuf[comrxbuf_rd_pos] == 0x0d))        
        {
            if(AtCmdBufDataSize == 1)
            {
                AtCmdBufDataSize = 0;
            }
            else//found one cmd
            {
                AtCmdBuf[AtCmdBufDataSize-1] = '\0';
                AtCmdPreParser(AtCmdBuf,AtCmdBufDataSize-1);
                
                AtCmdBufDataSize = 0;//move to next cmd if any
            }
        }
        
        comrxbuf_rd_pos ++;
        comrxbuf_rd_pos %= MAX_SIZE; //com buff len
    }
}


static void at_blesend(void)
{
    if (!GetConnectedStatus())
    {
        sent_num = send_len = 0;
        return;
    }
    
    if(send_len > sent_num)
    {
        sent_num += sconn_notifydata(send_data+sent_num, (((send_len - sent_num) > 20)? 20:(send_len - sent_num)));
        if(sent_num == send_len) sent_num = send_len = 0;
    }
    else if(send_len < sent_num)
    {
        sent_num += sconn_notifydata(send_data+sent_num, (((MAX_SEND_LEN - sent_num) > 20)? 20:(MAX_SEND_LEN - sent_num)));
    }
    
    sent_num %= MAX_SEND_LEN;
}

void CheckComPortInData(void) //at cmd NOT supported
{
    u16 send = 0;
    
    if(comrxbuf_wr_pos != comrxbuf_rd_pos)//not empty
    {
        if(!GetConnectedStatus())
        {
            comrxbuf_rd_pos = comrxbuf_wr_pos; //empty the buffer if any
        }
        else //connected
        {
            if(comrxbuf_wr_pos > comrxbuf_rd_pos)
            {
                send = sconn_notifydata(rxBuf+comrxbuf_rd_pos,comrxbuf_wr_pos - comrxbuf_rd_pos);
                comrxbuf_rd_pos += send;
            }
            else 
            {
                send = sconn_notifydata(rxBuf+comrxbuf_rd_pos,MAX_SIZE - comrxbuf_rd_pos);
                comrxbuf_rd_pos += send;
                comrxbuf_rd_pos %= MAX_SIZE;
            }
        }
    }
}

void UsrProcCallback(void)
{
    static uint8_t cnt_disc = 0;
    
    LPTIM_TimeOut_Reset();
    
    if (AtcmdFlag)
    {
        CheckAtCmdInfo();
        at_blesend();
    }
    else
    {
        CheckComPortInData();
        if( (GPIO_PIN_RESET == HAL_GPIO_ReadPin(WAKEUP_GPIO,WAKEUP_PIN))
           &&(GetConnectedStatus())
           &&(0 == cnt_disc))
        {
            cnt_disc = 1;
            ble_disconnect();
        }
        else
        {
            cnt_disc = 0;
        }
    }
    
    if ((txLen) && (0 == PosW))
    {
        UARTx->SBUF = txBuf[PosW++];
    }
}
