#include "BSP.h"
#include "mg_api.h"


#ifdef MS1658_BOARD
    #define TEST_RTC_CLOCK 	RTC_CLOCK_LXT // RTC clock select
#else
    #define TEST_RTC_CLOCK 	RTC_CLOCK_LIRC
#endif

RTC_HandleTypeDef rtc_test = {0};
RTC_TimeTypeDef sTime_test = {0};
RTC_DateTypeDef sDate_test = {0};

void RTCClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};	
    if(TEST_RTC_CLOCK == RTC_CLOCK_LXT)
    {
        RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LXT;
        RCC_OscInitStruct.LXTState = RCC_LXT_ON;
        if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
        {
            Error_Handler();
        }
    }
    else if(TEST_RTC_CLOCK == RTC_CLOCK_LIRC)
    {
        RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LIRC;
        RCC_OscInitStruct.LIRCState = RCC_LIRC_ON;
        RCC_OscInitStruct.LIRCCalibrationValue = RCC_LIRCCALIBRATION_32K;
        if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
        {
            Error_Handler();
        }
    }
    __HAL_RCC_RTC_CLK_ENABLE();
}

void RTC_Init(void)
{
    /*set init handle*/
    rtc_test.Instance = RTC;
    rtc_test.Init.ClockSource = TEST_RTC_CLOCK;	
    rtc_test.Init.HourFormat = RTC_HOURFORMAT_12;//RTC_HOURFORMAT_12;
    rtc_test.Init.TimeAdjustMode = RTC_TIME_ADJUST_SEC30;
    rtc_test.Init.TimeTrim = 0X00;
        
    rtc_test.State = HAL_RTC_STATE_RESET;
    HAL_RTC_Init(&rtc_test);
    /*set time and date */
    sTime_test.Hours = 23;	
    sTime_test.Minutes = 59;
    sTime_test.Seconds = 30;
    sDate_test.Date = 31;
    sDate_test.Month = 12;
    sDate_test.Year = 2020;
    sDate_test.WeekDay = RTC_WEEKDAY_THURSDAY;
    //	HAL_RTC_SetTime(&rtc_test, &sTime_test, rtc_test.Init.HourFormat);
    //	HAL_RTC_SetDate(&rtc_test, &sDate_test);
    HAL_RTC_SetTime_SetDate(&rtc_test, &sTime_test, rtc_test.Init.HourFormat, &sDate_test);
}

unsigned char *ble_mac_addr;

const unsigned char AdvDat_HRS[]=
{
    0x02,0x01,0x06,
    0x03,0x19,0x41,0x03,
    0x05,0x03,0x0D,0x18,0x0A,0x18
};

int main( void )
{
    unsigned char * const ft_val = (unsigned char *)0x18000040;
    unsigned char ft_value[2] = {0xc0, 0x12};
    unsigned short reason = 0;
   	RTC_TimeTypeDef sTime_current = {0};
    RTC_DateTypeDef sDate_current = {0};
    
    BSP_Init();
    
    LogInit();
    printf("\nUART1, PD5-TXD, PD6-RXD");
    reason = RCC->RSTSR;
    printf("\nStart:0x%02x", reason);
    __HAL_RCC_CLEAR_RESET_FLAGS(reason);
    
    RTCClock_Config();
	RTC_Init();
    printf("\nTest rtc function!!");
    
    SetBleIntRunningMode();
    radio_initBle(TXPWR_0DBM, &ble_mac_addr);
    
    if((*ft_val > 11) && (*ft_val < 25)){
        ft_value[1] = *ft_val;
        mg_activate(0x53);
        mg_writeBuf(0x4, ft_value, 2);
        mg_activate(0x56);
    }
    
    ble_set_adv_data((unsigned char *)AdvDat_HRS, sizeof(AdvDat_HRS));
    
    ble_run_interrupt_start(160*2); //320*0.625=200 ms
    
    while(1)
    {
//        IrqMcuGotoSleepAndWakeup(); //lowpower will impact uart print
        
        HAL_RTC_GetTime_Date(&rtc_test, &sTime_current, &sDate_current);
        if(rtc_test.Init.HourFormat == RTC_HOURFORMAT_12)
        {
            if(sTime_current.Hours>=12)
            {   
                printf("NOW Time is:%d-%d-%d PM %d:%d:%d \r\n",sDate_current.Year,sDate_current.Month,	\
                    sDate_current.Date,(sTime_current.Hours-12),sTime_current.Minutes,sTime_current.Seconds);
            }
            else
            {
                printf("NOW Time is:%d-%d-%d AM %d:%d:%d \r\n",sDate_current.Year,sDate_current.Month,	\
                    sDate_current.Date,sTime_current.Hours,sTime_current.Minutes,sTime_current.Seconds);
            }
        }
        else
        {
            printf("NOW Time is:%d-%d-%d  %d:%d:%d \r\n",sDate_current.Year,sDate_current.Month,	\
                sDate_current.Date,sTime_current.Hours,sTime_current.Minutes,sTime_current.Seconds);
        }
        
        LED_Toggle();
        HAL_Delay(1000);
    }
}
