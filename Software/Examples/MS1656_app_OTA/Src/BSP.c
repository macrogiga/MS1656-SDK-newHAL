/*
    Copyright (c) 2020 Shanghai Macrogiga Electronics Co., Ltd.

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies 
    of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
    DEALINGS IN THE SOFTWARE.
*/
#include "BSP.h"
#include "mg_api.h"


UART_HandleTypeDef sUartxHandle = {0};

void GPIO_Init(void)
{
    GPIO_InitTypeDef gpioinitstruct={0};

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
#ifdef MS1658_BOARD
    __HAL_RCC_GPIOD_CLK_ENABLE();
#endif
    
    gpioinitstruct.Pin    = LED_PIN ;
    gpioinitstruct.Mode = GPIO_MODE_OUTPUT;
    gpioinitstruct.OpenDrain = GPIO_PUSHPULL;
    gpioinitstruct.Debounce.Enable = GPIO_DEBOUNCE_DISABLE;
    gpioinitstruct.SlewRate = GPIO_SLEW_RATE_LOW;
    gpioinitstruct.DrvStrength = GPIO_DRV_STRENGTH_HIGH;
    gpioinitstruct.Pull = GPIO_PULLUP;//GPIO_NOPULL;
    HAL_GPIO_Init(LED_GPIO, &gpioinitstruct);
    
    gpioinitstruct.Pin = IRQ_PIN;
    gpioinitstruct.Pull = GPIO_PULLUP;
    gpioinitstruct.Debounce.Enable = GPIO_DEBOUNCE_DISABLE;
    gpioinitstruct.SlewRate = GPIO_SLEW_RATE_HIGH;
    gpioinitstruct.DrvStrength = GPIO_DRV_STRENGTH_HIGH;
    /* Configure Button pin as input */
    gpioinitstruct.Mode   = GPIO_MODE_INPUT;
    HAL_GPIO_Init(IRQ_GPIO, &gpioinitstruct);

    /* Configure Button pin as input with External interrupt */
    gpioinitstruct.Mode   = EXTI_MODE;
    gpioinitstruct.Exti.Enable = GPIO_EXTI_INT_ENABLE;
    gpioinitstruct.Exti.EdgeLevelSel = GPIO_EXTI_INT_EDGE;
    gpioinitstruct.Exti.RiseFallSel = GPIO_EXTI_INT_LOWFALL;
    HAL_GPIO_Init(IRQ_GPIO, &gpioinitstruct);

    __HAL_GPIO_EXTI_CLEAR_FLAG(IRQ_GPIO, IRQ_PIN);
    /* Enable and set Button EXTI Interrupt to the lowest priority */
#ifdef MS1658_BOARD
    HAL_NVIC_SetPriority(GPIOD_IRQn, PRIORITY_LOW);
    HAL_NVIC_EnableIRQ(GPIOD_IRQn);
#else //default
    HAL_NVIC_SetPriority(GPIOB_IRQn, PRIORITY_LOW);
    HAL_NVIC_EnableIRQ(GPIOB_IRQn);
#endif

}

void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};	
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HIRC;
    RCC_OscInitStruct.HIRCState = RCC_HIRC_ON;
    RCC_OscInitStruct.HIRCCalibrationValue = RCC_HIRCCALIBRATION_24M;
    
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    
    /**Initializes the CPU, AHB and APB busses clocks 
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HIRC;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APBCLKDivider = RCC_PCLK_DIV1;
    
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
}

void BSP_Init(void)
{
    HAL_Init();
    SystemClock_Config();
    SPIM_Init();
    GPIO_Init();
    LPTIM_Init(LIRC_VALUE);
}


void LED_ONOFF(unsigned char onFlag)//module indicator,GPA8
{
    if(onFlag){
        HAL_GPIO_WritePin(LED_GPIO,LED_PIN,GPIO_PIN_SET);//high, on
    }else{
        HAL_GPIO_WritePin(LED_GPIO,LED_PIN,GPIO_PIN_RESET);//low, off
    }
}


void IrqMcuGotoSleepAndWakeup(void)
{
    if(ble_run_interrupt_McuCanSleep())
    {
        /*SCB->SCR |= 0x04;
        __WFI();*/
    }
}

/*
0~3KB BootCode
3~4KB UserData
4~34KB RunCode
34~64KB OTA
*/
#define FLASH_E2PROM_ADDR_BASE (0x00000000)
//#define FLASH_BOOT_ROM_SIZE    (3*1024)
//#define FLASH_E2PROM_ADDR_USR  (FLASH_E2PROM_ADDR_BASE + FLASH_BOOT_ROM_SIZE)
#define FLASH_E2PROM_ADDR_OTA  (FLASH_E2PROM_ADDR_BASE + 34*1024)

#define APPLICATION_ADDRESS     (uint32_t)0x00001010


void OtaSystemReboot(void)//porting api
{
    NVIC_SystemReset();
}

u32 GetOtaAddr(void)
{
    return FLASH_E2PROM_ADDR_OTA;
}


void WriteFlashE2PROM(u8* data, u16 len, u32 pos, u8 flag) //4 bytes aligned
{
    u32 t;
    
    while(len >= 4)
    {
        t = data[3]; t <<= 8;
        t |= data[2]; t <<= 8;
        t |= data[1]; t <<= 8;
        t |= data[0];
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,pos, t);
        pos += 4;
        len -= 4;
        data += 4;
    }
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
